<?php
	abstract class Debugtools {
		public static function trace($x, $die = true) {
			echo "<pre>";
			print_r($x);
			if($die) die();
			echo "</pre>";
		}
	}
?>