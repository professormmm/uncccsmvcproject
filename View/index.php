<p><h1>Students List</h1></p>
<table class="table">
	<tr>
		<td>ID</td>
		<td>Lastname</td>
		<td>Firstname</td>
		<td>Middlename</td>
		<td>Birthdate</td>
		<td>Gender</td>
	</tr>
	<?php
		while ($student = mysqli_fetch_object($result)) {
       		echo "<tr><td>" . $student->student_id . "</td>";
       		echo "<td>" . $student->student_lastname . "</td>";
       		echo "<td>" . $student->student_firstname . "</td>";
       		echo "<td>" . $student->student_midname . "</td>";
       		echo "<td>" . $student->student_dob . "</td>";
       		echo "<td>" . $student->student_gender . "</td>";
       		echo "</tr>";
   		}   
    ?>
</table>
<script type="text/javascript">
	
</script>